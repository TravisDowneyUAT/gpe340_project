using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncendiaryAmmoPickup : MonoBehaviour
{
    public Weapon weapon;
    public IncendiaryAmmo pickup;

    private void OnTriggerEnter(Collider col)
    {
        col.gameObject.tag.Equals("Player");

        weapon = col.GetComponentInChildren<Rifle>();

        weapon.damage += pickup.damageIncrease;

        Destroy(gameObject);
    }
}
