using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupManager : MonoBehaviour
{
    public List<Pickups> pickups;

    public Weapon weapon;

    private PlayerStats stats;

    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        List<Pickups> pickupsToRemove = new List<Pickups>();

        //Go through all the pickups
        foreach (Pickups pickup in pickups)
        {
            //subtract the last frame draw time
            pickup.countdown -= Time.deltaTime;

            //if the pickup timer reaches 0 then remove it
            if (pickup.countdown <= 0)
            {
                pickupsToRemove.Add(pickup);
            }
        }

        //Remove the pickup
        foreach(Pickups pickup in pickups)
        {
            RemovePickup(pickup);
        }

    }

    public void AddPickup(Pickups pickupToAdd)
    {
        Pickups newPickup = new Pickups();

        if(pickupToAdd.pickupType == Pickups.PickupType.IncendiaryAmmo)
        {
            newPickup = new IncendiaryAmmo((IncendiaryAmmo)pickupToAdd);
            newPickup.OnAddAmmoPickup(weapon);
        }
        if (pickupToAdd.pickupType == Pickups.PickupType.HealthPack)
        {
            newPickup = new HealthPack((HealthPack)pickupToAdd);
            newPickup.OnAddStatPickup(stats);
        }

        pickups.Add(newPickup);
    }

    public void RemovePickup(Pickups pickupToRemove)
    {
        pickups.Remove(pickupToRemove);

        if(pickupToRemove.pickupType == Pickups.PickupType.IncendiaryAmmo)
        {
            pickupToRemove.OnRemoveAmmoPickup(weapon);
        }
    }
}
