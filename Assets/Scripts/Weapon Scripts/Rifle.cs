using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : Weapon
{
    public GameObject bulletPrefab; //reference to the bullet that will be fired
    public Transform muzzlePoint; //reference to the muzzle point on our rifle
    public float shotCD; //float for shooting cooldown
    float shotTime; //when the gun can shoot again
    float bulletSpeed = 5f; //speed at which the bullet will travel
        
    // Start is called before the first frame update
    public override void Start()
    {
        shotTime = Time.time;
        damage = 10f;
        base.Start();
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void AttackStart()
    {
        base.AttackStart();
    }

    public override void AttackEnd()
    {
        base.AttackEnd();
    }

    public void ShootBullet()
    {
        if (Time.time >= shotTime)
        {
            //Shoot a bullet from the muzzle of the gun
            GameObject bullet = Instantiate(bulletPrefab, muzzlePoint.position, muzzlePoint.rotation) as GameObject;
            
            //Get the script component from the bullet
            Bullet bulletScript = bullet.GetComponent<Bullet>();

            //Set the bulletScript variables to the appropriate variables in this script
            bulletScript.damage = damage;
            bulletScript.moveSpeed = bulletSpeed;

            //delay the next shot and begin the cooldown timer
            shotTime = Time.time + shotCD;
        }
    }
}
