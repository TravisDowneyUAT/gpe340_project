using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pawn : MonoBehaviour
{
    public Weapon weapon;

    // Start is called before the first frame update
    public virtual void Start()
    {
        
    }

    // Update is called once per frame
    public virtual void Update()
    {
        
    }

    public virtual void Move(Vector3 moveDir)
    {

    }

    public virtual void RotateTowards(Vector3 targetPoint)
    {

    }
}
