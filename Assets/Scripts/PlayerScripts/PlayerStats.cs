using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour
{
    public int lives;

    private float maxHealth = 100f;
    private GameManager gm;

    [Header("Do not change")]
    public float curHealth;

    private HealthBar healthBar;

    // Start is called before the first frame update
    void Start()
    {
        curHealth = maxHealth;
        healthBar = GameObject.FindGameObjectWithTag("PlayerHealth").GetComponent<HealthBar>();
        healthBar.SetMaxHealth(maxHealth);
        gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

   public void TakeDamage(float damage)
    {
        curHealth -= damage;

        if (curHealth <= 0)
        {
            Die();
        }

        Debug.Log(transform.name + " takes " + damage + " damage");

        healthBar.SetHealth(curHealth);
    }

    void Die()
    {
        lives--;
        gm.livesText.text = "Lives: " + lives;
        curHealth = 100f;
        healthBar.SetHealth(curHealth);
        if (lives <= 0)
        {
            Destroy(gameObject);

            SceneManager.LoadScene("GameOver");
        }
    }
}
