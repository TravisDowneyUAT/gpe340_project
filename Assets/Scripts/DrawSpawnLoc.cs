using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSpawnLoc : MonoBehaviour
{
    public Vector3 gizmoScale;

    private void OnDrawGizmos()
    {
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.color = Color.Lerp(Color.green, Color.clear, 0.5f);
        Gizmos.DrawCube(Vector3.up * gizmoScale.y / 2f, gizmoScale);
        Gizmos.DrawRay(Vector3.zero, Vector3.forward * 0.4f);
    }


}
