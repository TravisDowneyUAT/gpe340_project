using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HealthPack : Pickups
{
    public float restoredHealth;
    public bool isPermanent;

    public HealthPack(HealthPack pickupToClone)
    {
        restoredHealth = pickupToClone.restoredHealth;
        countdown = pickupToClone.countdown;
        isPermanent = pickupToClone.isPermanent;

        pickupType = PickupType.HealthPack;
    }

    public override void OnAddStatPickup(PlayerStats stats)
    {
        stats.curHealth += restoredHealth;

        base.OnAddStatPickup(stats);
    }

    public override void OnRemoveStatPickup(PlayerStats stats)
    {
        if (isPermanent)
        {
            return;
        }

        base.OnRemoveStatPickup(stats);
    }
}
