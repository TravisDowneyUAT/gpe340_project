using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class IncendiaryAmmo : Pickups
{
    public float damageIncrease;
    public bool isPermanent;

   public IncendiaryAmmo(IncendiaryAmmo pickupToClone)
    {
        damageIncrease = pickupToClone.damageIncrease;
        countdown = pickupToClone.countdown;
        isPermanent = pickupToClone.isPermanent;

        pickupType = PickupType.IncendiaryAmmo;
    }

    public override void OnAddAmmoPickup(Weapon weapon)
    {
        weapon.damage += damageIncrease;

        base.OnAddAmmoPickup(weapon);
    }

    public override void OnRemoveAmmoPickup(Weapon weapon)
    {
        if (isPermanent)
        {
            return;
        }

        weapon.damage -= damageIncrease;

        base.OnRemoveAmmoPickup(weapon);
    }
}
