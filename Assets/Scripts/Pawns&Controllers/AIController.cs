using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : Controller
{
    public Weapon weapon;

    Rigidbody rb;
    Collider capsule;
    Animator anim;

    public GameObject target;
    NavMeshAgent agent;

    // Start is called before the first frame update
    public override void Start()
    {
        rb = GetComponent<Rigidbody>();
        capsule = GetComponent<Collider>();
        agent = GetComponent<NavMeshAgent>();
        anim = pawn.GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    public override void Update()
    {
        if (GameManager.isPaused == true)
        {
            return;
        }
        agent.SetDestination(target.transform.position);

        //Get the way the agent wants to move
        Vector3 input = agent.desiredVelocity;

        //change it so it can work with the animator
        input = transform.InverseTransformDirection(input);

        //pass the values into the animator
        pawn.Move(agent.desiredVelocity);

        agent.isStopped = true;
        //if the agent has stopped moving, then start shooting otherwise just follow the player
        if (agent.isStopped == true)
        {
            weapon.AttackStart();
            agent.isStopped = false;
        }
        if (agent.isStopped == false)
        {
            weapon.AttackEnd();
        }
    }

    private void OnAnimatorMove()
    {
        agent.velocity = anim.velocity;
    }

    public void DeactivateRagdoll()
    {
        Debug.Log("Ragdoll deactivated");

        int i;
        //Get all the Rigidbody components on the joints
        Rigidbody[] childRBs = GetComponentsInChildren<Rigidbody>();
        for(i=0; i < childRBs.Length; i++)
        {
            childRBs[i].isKinematic = true;
        }

        //Get all the Collider components on the joints
        Collider[] childColliders = GetComponentsInChildren<Collider>();
        for (i = 0; i < childColliders.Length; i++)
        {

            childColliders[i].enabled = false;
        }

        rb.isKinematic = false;
        capsule.enabled = true;
        anim.enabled = true;
    }

    public void ActivateRagdoll()
    {
        Debug.Log("Ragdoll activated");

        int i;
        Rigidbody[] childRBs = GetComponentsInChildren<Rigidbody>();
        for (i = 0; i < childRBs.Length; i++)
        {
            childRBs[i].isKinematic = false;
        }

        Collider[] childColliders = GetComponentsInChildren<Collider>();
        for (i = 0; i < childColliders.Length; i++)
        {
            childColliders[i].enabled = true;
        }


        rb.isKinematic = true;
        capsule.enabled = false;
        anim.enabled = false;
    }

}
