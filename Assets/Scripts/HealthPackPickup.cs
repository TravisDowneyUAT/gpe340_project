using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPackPickup : MonoBehaviour
{
    PlayerStats stats;
    public HealthPack healthPack;
    private void OnTriggerEnter(Collider col)
    {
        col.gameObject.tag.Equals("Player");

        stats = col.GetComponent<PlayerStats>();

        stats.curHealth += healthPack.restoredHealth;

        Destroy(gameObject);
    }
}
