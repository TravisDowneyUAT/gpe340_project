using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn
{
    private Animator animator;
    public float moveSpeed;
    private float rotateSpeed = 90f;
    bool isCrouching = false;
    private AudioManager manager;

    // Start is called before the first frame update
    public override void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    public override void Update()
    {
        
    }

    public override void Move(Vector3 moveDir)
    {
        //Make it fair for gamepad users
        moveDir.Normalize();

        //Convert moveDir from local movement to world movement
        moveDir = transform.InverseTransformDirection(moveDir);

        animator.SetFloat("Forward", moveDir.z * moveSpeed);
        animator.SetFloat("Right", moveDir.x * moveSpeed);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isCrouching = true;
            animator.SetBool("isCrouching", isCrouching);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isCrouching = false;
            animator.SetBool("isCrouching", isCrouching);
        }

        base.Move(moveDir);
    }

    public override void RotateTowards(Vector3 targetPoint)
    {
        //Rotate towards target point
        Quaternion targetRotation;
        Vector3 toTarget = targetPoint - transform.position;
        targetRotation = Quaternion.LookRotation(toTarget, Vector3.up);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);

        base.RotateTowards(targetPoint);
    }

    public void OnAnimatorIK(int layerIndex)
    {
        //If the player has no weapon, then don't do Inverse Kinematics
        if (!weapon)
        {
            return;
        }
        if(weapon.leftHand && weapon.rightHand)
        {
            //Setting models hand positions based on the hand points on the weapon
            animator.SetIKPosition(AvatarIKGoal.LeftHand, weapon.leftHand.position);
            animator.SetIKPosition(AvatarIKGoal.RightHand, weapon.rightHand.position);
            animator.SetIKRotation(AvatarIKGoal.LeftHand, weapon.leftHand.rotation);
            animator.SetIKRotation(AvatarIKGoal.RightHand, weapon.rightHand.rotation);

            //Setting the weight of the positions
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
        }
        else
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0f);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0f);
        }
    }
}
