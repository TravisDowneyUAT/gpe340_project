using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float damage;
    public float moveSpeed;

    public float bulletLifespan = 1.5f; //How long the bullets lives for if it doesn't hit anything

    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, bulletLifespan);
        rb = GetComponent<Rigidbody>();

        //Add force to the bullet to make it move
        rb.velocity = transform.forward * moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider col)
    {
        GameObject collided = col.gameObject;
        EnemyStats collidedHealth = collided.GetComponent<EnemyStats>();
        PlayerStats collidedPlayer = collided.GetComponent<PlayerStats>();

        //If the object has an EnemyStats script then run the TakeDamage method on that object
        if (collidedHealth != null)
        {
            collidedHealth.TakeDamage(damage);
        }

        if (collidedPlayer != null)
        {
            collidedPlayer.TakeDamage(damage);
        }

        Destroy(this.gameObject);
    }
}
