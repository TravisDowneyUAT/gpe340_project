using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pickups
{
    public enum PickupType
    {
        IncendiaryAmmo,
        HealthPack
    }

    public float countdown;

    public Pickups() { }

    public PickupType pickupType;

    public virtual void OnAddAmmoPickup(Weapon weapon)
    {

    }

    public virtual void OnRemoveAmmoPickup(Weapon weapon)
    {

    }

    public virtual void OnAddStatPickup(PlayerStats stats)
    {

    }

    public virtual void OnRemoveStatPickup(PlayerStats stats)
    {

    }
}
