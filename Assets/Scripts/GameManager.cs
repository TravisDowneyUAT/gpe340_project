using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public PlayerStats playerStats;
    public EnemySpawner spawner;

    public static bool isPaused = false;
    private int playerLives;    
    
    public GameObject pauseMenu;

    public Text livesText;
    
    public bool isEasy;
    public bool isMedium;
    public bool isHard;
    
    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        playerLives = playerStats.lives;
        livesText.text = "Lives: " + playerLives;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused == true)
            {
                //Resume the game
                Resume();
            }
            else
            {
                Debug.Log("Pausing Game");
                Pause();
            }
        }

        if (spawner.curActiveNME == 0)
        {
            WinGame();
        }
    }

    public void Pause()
    {
        pauseMenu.SetActive(true);
        isPaused = true;
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        isPaused = false;
        Time.timeScale = 1f;
    }

    void WinGame()
    {
        SceneManager.LoadScene("GameComplete");
        spawner.SetActiveNMEs(-1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
