using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIPawn : Pawn
{
    public float lookRadius;
    Animator anim;

    Transform target;
    NavMeshAgent agent;
    Vector3 desiredVelocity;

    float moveSpeed;

    // Start is called before the first frame update
    public override void Start()
    {
       // target = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        moveSpeed = agent.speed;
    }

    // Update is called once per frame
    public override void Update()
    {
        //Get the distance from our enemy to the player
       /* float distance = Vector3.Distance(target.position, transform.position);

        desiredVelocity = Vector3.MoveTowards(desiredVelocity, agent.desiredVelocity, agent.acceleration * Time.deltaTime);

        Vector3 input = transform.InverseTransformDirection(desiredVelocity);
        anim.SetFloat("Forward", input.x);
        anim.SetFloat("Right", input.z);

        //If the player is within our look radius then move towards them
        if (distance <= lookRadius)
        {
            agent.SetDestination(target.position);

            //If the player is within range then attack them
            if (distance <= agent.stoppingDistance)
            {
                //Attack the player
                weapon.AttackStart();
            }
            //Stop attacking the player
            weapon.AttackEnd();

            //Face the player
            FaceTarget();
        }*/
    }

    public override void Move(Vector3 moveDir)
    {
        //Make it fair for gamepad users
        moveDir.Normalize();

        //Convert moveDir from local movement to world movement
        moveDir = transform.InverseTransformDirection(moveDir);

        anim.SetFloat("Forward", moveDir.z * moveSpeed);
        anim.SetFloat("Right", moveDir.x * moveSpeed);

        

        base.Move(moveDir);
    }

    void FaceTarget()
    {
        Vector3 dir = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(dir.x, 0, dir.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime);
    }

    public void OnAnimatorIK(int layerIndex)
    {
        //If the enemy has no weapon, then don't do Inverse Kinematics
        if (!weapon)
        {
            return;
        }
        if (weapon.leftHand && weapon.rightHand)
        {
            //Setting models hand positions based on the hand points on the weapon
            anim.SetIKPosition(AvatarIKGoal.LeftHand, weapon.leftHand.position);
            anim.SetIKPosition(AvatarIKGoal.RightHand, weapon.rightHand.position);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, weapon.leftHand.rotation);
            anim.SetIKRotation(AvatarIKGoal.RightHand, weapon.rightHand.rotation);

            //Setting the weight of the positions
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
        }
        else
        {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0f);
        }
    }
}
