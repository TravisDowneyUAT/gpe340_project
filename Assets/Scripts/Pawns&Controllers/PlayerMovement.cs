using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class PlayerMovement : Controller
{
    public Weapon weapon;

    private Camera cam;

    private AudioManager manager;
   
    public override void Start()
    {
        cam = Camera.main;
        manager = FindObjectOfType<AudioManager>();
        base.Start();
    }

    public override void Update()
    {
        pawn.Move(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));

        //Every frame, tell player to rotate towards mouse position
        pawn.RotateTowards(GetMousePos());

        if (Input.GetButtonDown("Fire1"))
        {
            weapon.AttackStart();
            manager.Play("Gunshot");
        }
        if (Input.GetButtonUp("Fire1"))
        {
            weapon.AttackEnd();
        }
    }

    public Vector3 GetMousePos()
    {
        Vector3 mousePoint = new Vector3();

        //Find the plane that the player is on
        Plane characterPlane = new Plane(Vector3.up, pawn.transform.position);

        //Send out a ray from the camera down to the characterPlane
        Ray rotationRay = new Ray();
        rotationRay = cam.ScreenPointToRay(Input.mousePosition);

        float hit;
        if (characterPlane.Raycast(rotationRay, out hit))
        {
            mousePoint = rotationRay.GetPoint(hit);
        }
        return mousePoint;
    }
}
