using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Weapon : MonoBehaviour
{
    [Header("HandPoints")]
    public Transform rightHand;
    public Transform leftHand;

    [Header("Events")]
    public UnityEvent OnAttackStart;
    public UnityEvent OnAttackEnd;

    public ParticleSystem muzzleFlash;
    
    public float damage;

    // Start is called before the first frame update
    public virtual void Start()
    {
        
    }

    // Update is called once per frame
    public virtual void Update()
    {
        
    }

    public virtual void AttackStart()
    {
        //What happens when player pulls trigger/starts any attack
        OnAttackStart.Invoke();
        muzzleFlash.Play();
    }

    public virtual void AttackEnd()
    {
        //What happens when any attack ends
        OnAttackEnd.Invoke();
    }
}
