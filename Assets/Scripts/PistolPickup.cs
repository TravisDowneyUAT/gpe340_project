using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolPickup : MonoBehaviour
{
    public GameObject pistol;
    public GameObject player;
    PlayerMovement movement;
    PlayerPawn pawn;
    private void Start()
    {
        movement = player.GetComponent<PlayerMovement>();
        pawn = player.GetComponent<PlayerPawn>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            pistol.SetActive(true);

            movement.weapon = pistol.GetComponent<Pistol>();
            pawn.weapon = pistol.GetComponent<Pistol>();

            Destroy(gameObject);
        }
    }
}
