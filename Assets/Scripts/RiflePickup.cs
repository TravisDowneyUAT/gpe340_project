using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiflePickup : MonoBehaviour
{
    public GameObject rifle;
    public GameObject player;
    PlayerMovement movement;
    PlayerPawn pawn;
    private void Start()
    {
        movement = player.GetComponent<PlayerMovement>();
        pawn = player.GetComponent<PlayerPawn>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            rifle.SetActive(true);

            movement.weapon = rifle.GetComponent<Rifle>();
            pawn.weapon = rifle.GetComponent<Rifle>();

            Destroy(gameObject);
        }
    }
}
