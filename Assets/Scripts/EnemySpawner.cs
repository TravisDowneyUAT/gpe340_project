using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    public GameObject[] enemies;
    GameManager gm;

    [Header("Do Not Change")]
    public int curActiveNME;

    // Start is called before the first frame update
    void Start()
    {
        curActiveNME = -1;
        gm = GetComponent<GameManager>();
    }

    private void Update()
    {
       
    }

    public void SetActiveNMEs(int activeNME)
    {
        curActiveNME = activeNME;
        SpawnEnemy();
    }

    void SpawnEnemy()
    {
        
        if (curActiveNME == 3)
        {
            Debug.Log("Easy Mode Spawning Activated");
            for (int i = 0; i < curActiveNME; i++)
            {
                enemies[i].SetActive(true);
            }
        }
        if (curActiveNME == 5)
        {
            Debug.Log("Medium Mode Spawning Activated");
            for(int i = 0; i < curActiveNME; i++)
            {
                enemies[i].SetActive(true);
            }
        }
        if (curActiveNME == 7)
        {
            Debug.Log("Hard Mode Spawning Activated");
            for(int i = 0; i < curActiveNME; i++)
            {
                enemies[i].SetActive(true);
            }
        }
    }
}
