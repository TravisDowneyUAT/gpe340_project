using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class drop
{
    public GameObject itemToDrop; //the item that will be dropped when the enemy dies
    public int weight; //how likely it is for an object to drop
}

public class ItemDrops : MonoBehaviour
{
    public List<drop> itemDrops;
    private List<int> cdfArray;

    public void DropRndItem()
    {

        //choose random number less than total cdf
        int rndNum = Random.Range(0, cdfArray[cdfArray.Count - 1]);

        //look through the cdfArray using the rndNum
        int selectedIndex = System.Array.BinarySearch(cdfArray.ToArray(), rndNum);
        if (selectedIndex < 0)
        {
            selectedIndex = ~selectedIndex;
        }
        
        //spawn the chosen object
        Instantiate(itemDrops[selectedIndex].itemToDrop, transform.position, transform.rotation);
    }

    // Start is called before the first frame update
    void Start()
    {
        cdfArray = new List<int>();
        //go through every item in itemDrops
        for(int i = 0; i < itemDrops.Count; i++)
        {
            //add the weight to the cdf array
            if (i == 0)
            {
                cdfArray.Add(itemDrops[i].weight);
            }
            else
            {
                cdfArray.Add(itemDrops[i].weight + cdfArray[i - 1]);
            }
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
