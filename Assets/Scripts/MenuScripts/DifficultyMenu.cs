using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DifficultyMenu : MonoBehaviour
{
    public GameManager gm;
    public EnemySpawner spawner;

    public void SetDifficultyEasy(bool isEasy)
    {
        gm.isEasy = isEasy;
        spawner.SetActiveNMEs(3);
        SceneManager.LoadScene("MainGame");
    }

    public void SetDifficultyMedium(bool isMedium)
    {
        gm.isMedium = isMedium;
        spawner.SetActiveNMEs(5);
        SceneManager.LoadScene("MainGame");
    }

    public void SetDifficultyHard(bool isHard)
    {
        gm.isHard = isHard;
        spawner.SetActiveNMEs(7);
        SceneManager.LoadScene("MainGame");
    }
}
