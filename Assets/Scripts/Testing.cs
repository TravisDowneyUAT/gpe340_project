using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    public GameObject player;
    private PlayerStats stats;
    public GameObject enemy;
    private EnemyStats eStats;
    private ItemDrops itemDrops;

    private void Start()
    {
        stats = player.GetComponent<PlayerStats>();
        eStats = enemy.GetComponent<EnemyStats>();
        itemDrops = enemy.GetComponent<ItemDrops>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            stats.TakeDamage(50f);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            eStats.TakeDamage(30f);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            itemDrops.DropRndItem();
        }
    }
}
