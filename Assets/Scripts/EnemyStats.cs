using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyStats : MonoBehaviour
{
    private ItemDrops itemDrops;
    private float maxHealth = 100f;

    [Header("Do not change")]
    public float curHealth;

    //float deathTimer = 4f;
    AIController controller;
    NavMeshAgent agent;
    
    private EnemySpawner spawner;

    [Header("HealthBar stuff")]
    public Image healthBar;
    

    // Start is called before the first frame update
    void Start()
    {
        curHealth = maxHealth;
        controller = GetComponent<AIController>();
        agent = GetComponent<NavMeshAgent>();
        itemDrops = GetComponent<ItemDrops>();
        spawner = GameObject.FindGameObjectWithTag("GameController").GetComponent<EnemySpawner>();
    }

   public void TakeDamage(float damage)
    {
        curHealth -= damage;

        healthBar.fillAmount = curHealth / maxHealth;

        if (curHealth <= 0)
        {
            Die();
        }

        Debug.Log(transform.name + " takes " + damage + " damage");

        
    }

    void Die()
    {
        itemDrops.DropRndItem();
        controller.ActivateRagdoll();
        spawner.curActiveNME--;
        Debug.Log("Current active enemies: " + spawner.curActiveNME);
        Destroy(gameObject);
    }
}
